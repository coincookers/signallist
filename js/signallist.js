 function zeroPad(aNumber) {
     return ("0" + aNumber).slice(-2);
 };

 function formatDate(timeStamp) {
     var M = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
     var D = new Date(timeStamp); // 23 Aug 2016 16:45:59 <-- Desired format.
     return D.getDate() + " " + M[D.getMonth()] + " " + D.getFullYear() + " " + D.getHours() + ":" + zeroPad(D.getMinutes()) + ":" + zeroPad(D.getSeconds());
 };

 function getJSON(url, callback) {
     var xhr = new XMLHttpRequest();
     xhr.open('GET', url, true);
     xhr.responseType = 'json';
     xhr.onload = function() {
         var status = xhr.status;
         if (status === 200) {
             callback(null, xhr.response);
         }
         else {
             callback(status, xhr.response);
         }
     };
     xhr.send();
 };
 
 function updateTitle(profit){
     var title = document.title;
     if (title.indexOf('%') > 0){
         var oldProfit = parseFloat(title.split('%')[0]);
         if(profit > oldProfit){
             document.title = (profit > 0?'+':'') + profit + "% Profit from BuySignals";  
         }
     }
     else{
         document.title = (profit > 0?'+':'') + profit + "% Profit from BuySignals";
     }
 }

 function showChart(pair, chartDataUrl, buySignalDate, chartDivId, buyPriceDivId, profitDivId) {

     console.log('Showing chart for ' + pair + " at div " + chartDivId);

     getJSON(chartDataUrl, function(err, data) {
         if (err !== null) {
             console.log('ERROR calling ' + chartDataUrl + ': ' + err);
         }
         else {

             var buyPrice = 0;
             var sellPrice = 0;
             var lossPrice = 0;
             var sellDate = buySignalDate;
             var lossDate = buySignalDate;

             var graphData = "Date,Price\n";
             for (var i in data) {
                 var item = data[i];
                 var date = new Date(item['date'] * 1000);
                 var dateStr = new Date(item['date'] * 1000).toISOString();
                 var price = item['high'];
                 if (date > buySignalDate && buyPrice == 0) {
                     buyPrice = price;
                 }
                 if (price > sellPrice || sellPrice == 0) {
                     sellPrice = price;
                     sellDate = date;
                 }
                 else if (price < buyPrice && date > buySignalDate){
                     if(lossPrice > buyPrice - price || lossPrice == 0){
                         lossPrice = buyPrice - price;
                         lossDate = date;
                     }
                 }
                 graphData = graphData + dateStr + ',' + price + "\n";
             }
             
             document.getElementById(buyPriceDivId).innerHTML = buyPrice;

             //console.log('graphdata for ' + chartDivId + ': ' + graphData);
             var g = new Dygraph(document.getElementById(chartDivId),
                 // For possible data formats, see http://dygraphs.com/data.html
                 // The x-values could also be dates, e.g. "2012/03/15"
                 graphData, {
                     // options go here. See http://dygraphs.com/options.html
                     legend: 'always',
                     height: 180,
                     width: 440,
                     series: {
                         Price: { axis: 'y2' },

                     },
                     axes: {
                         y: {
                             drawGrid: true,
                             independentTicks: false
                         },
                         y2: {
                             drawGrid: true,
                             independentTicks: true
                         }
                     }
                 });

             var min = buySignalDate.getMinutes();
             if (min % 10 < 5) {
                 min = Math.floor(min / 10) * 10;
             }
             else {
                 min = Math.floor(min / 10) * 10 + 5;
             }
             buySignalDate.setMinutes(min);
             buySignalDate.setSeconds(0);
             buySignalDate.setMilliseconds(0);
             console.log('Setting annotation for date ' + buySignalDate.toISOString() + ' with current minute ' + buySignalDate.getMinutes());

             var annotations = [{
                 'series': "Price",
                 'x': buySignalDate.toISOString(),
                 'shortText': "B",
                 'text': "BUY SIGNAL",
                 'cssClass': 'dygraph-annotation-buy'
             }];
             if (sellDate > buySignalDate) {
                 annotations.push({
                     'series': "Price",
                     'x': sellDate.toISOString(),
                     'shortText': "S",
                     'text': "SELL SIGNAL",
                     'cssClass': 'dygraph-annotation-sell-profit'
                 });
             }
             else if (lossDate > buySignalDate){
                 annotations.push({
                     'series': "Price",
                     'x': lossDate.toISOString(),
                     'shortText': "S",
                     'text': "SELL SIGNAL",
                     'cssClass': 'dygraph-annotation-sell-loss'
                 });
             }
             g.ready(function() {
                 g.setAnnotations(annotations);
             });

             if (sellDate > buySignalDate) {
                 var profit = ((sellPrice - buyPrice) / sellPrice * 100).toFixed(2)
                 document.getElementById(profitDivId).innerHTML = '<h1 class="positive_change">' + profit + '%</h1>';
                 updateTitle(profit, '+');
             }
             else if (lossDate > buySignalDate){
                 var profit = (lossPrice / buyPrice * 100).toFixed(2)
                 document.getElementById(profitDivId).innerHTML = '<h1 class="negative_change">' + profit + '%</h1>';
                 updateTitle(profit * -1, '-');
             }
         }
     });
 };


 function addToSignalList(pair, date, source, tableBodyId) {

     console.log("Pair: " + pair + ", date: " + date)

     var dateParts = date.split(' ')
     var buySignalDate = new Date(dateParts[0] + 'T' + dateParts[1] + 'Z')
     var timestampNow = new Date().getTime();
     var timestampSignal = buySignalDate.getTime();

     var tr = document.createElement("tr")

     var tdPair = document.createElement("td");
     tdPair.innerHTML = '<a href="https://poloniex.com/exchange#' + pair + '" target="_blank">' + pair + '</a>';

     var tdDate = document.createElement("td");
     tdDate.innerHTML = formatDate(timestampSignal);

     var tdPrice = document.createElement("td");
     var buyPriceDiv = document.createElement("div");
     var buyPriceDivId = 'buyprice_' + pair + '_' + timestampSignal;
     buyPriceDiv.setAttribute('id', buyPriceDivId);
     tdPrice.appendChild(buyPriceDiv);

     var tdProfit = document.createElement("td");
     var profitDiv = document.createElement("div");
     var profitDivId = 'profit_' + pair + '_' + timestampSignal;
     profitDiv.setAttribute('id', profitDivId);
     tdProfit.appendChild(profitDiv);

     var tdLabels = document.createElement("td")
     var labelSourceDiv = document.createElement("div");
     labelSourceDiv.setAttribute('id', 'source_' + pair + '_' + timestampSignal);
     labelSourceDiv.setAttribute('class', 'labelDiv');
     labelSourceDiv.innerHTML = source;
     tdLabels.appendChild(labelSourceDiv);

     var tdChart = document.createElement("td")

     var chartDiv = document.createElement("div");
     var chartDivId = 'chart_' + pair + '_' + timestampSignal;
     chartDiv.setAttribute('id', chartDivId)
     chartDiv.style.float = 'left';
     tdChart.appendChild(chartDiv);


     var chartDataUFrom = timestampSignal - 1000 * 60 * 60 * 1;
     var chartDataUntil = timestampSignal + 1000 * 60 * 60 * 6;
     if (chartDataUntil > timestampNow) {
         chartDataUntil = timestampNow;
     }

     var chartDataUrl = 'https://poloniex.com/public?command=returnChartData&currencyPair=' + pair + '&start=' + chartDataUFrom / 1000 + '&end=' + chartDataUntil / 1000 + '&period=300';
     console.log('Calling ' + chartDataUrl);
     showChart(pair, chartDataUrl, buySignalDate, chartDivId, buyPriceDivId, profitDivId);

     tr.appendChild(tdPair);
     tr.appendChild(tdDate);
     tr.appendChild(tdPrice);
     tr.appendChild(tdProfit);
     tr.appendChild(tdLabels);
     tr.appendChild(tdChart);

     document.getElementById(tableBodyId).appendChild(tr);
 };
 